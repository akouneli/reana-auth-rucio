ARG BASEIMAGE=rucio/rucio-clients
ARG BASETAG=release-1.29.1
FROM $BASEIMAGE:$BASETAG

USER root

# CERN cert (where is official place?)
RUN curl -Lo /etc/pki/tls/certs/CERN-bundle.pem https://gitlab.cern.ch/plove/rucio/-/raw/7121c7200257a4c537b56ce6e7e438f0b35c6e48/etc/web/CERN-bundle.pem

# Add the rucio configuration template
ADD --chown=user:user etc/rucio/rucio.cfg.j2 /opt/user/rucio.cfg.j2

USER user
WORKDIR /home/user
